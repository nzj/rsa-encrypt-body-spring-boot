package cn.shuibo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootplusApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootplusApplication.class, args);
        System.out.println("访问地址:http://127.0.0.1:8082/doc.html");
    }
}
