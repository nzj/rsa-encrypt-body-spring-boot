package cn.shuibo.controller;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONUtil;
import cn.shuibo.annotation.Decrypt;
import cn.shuibo.annotation.Encrypt;
import cn.shuibo.model.AjaxResult;
import cn.shuibo.model.IosNoticeEntity;
import cn.shuibo.model.PageQuery;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;
import java.util.Date;


/**
 * IOS通知消息表
 *
 * @author gyb
 * @email guoyb@mapbar.com
 * @date 2018-10-19 14:10:10
 */
@RestController
@Validated
@Slf4j
public class TestController {

    private static IosNoticeEntity entity;

    static {
        entity = new IosNoticeEntity();
        entity.setAddress("address测试");
        entity.setCreateTime(new Date());
        entity.setName(RandomUtil.randomString(5));
        entity.setId(RandomUtils.nextLong());
    }


    @GetMapping("/get/{id}")
    @Encrypt
    @ApiOperation(value = "根据id查询",notes = "返回加密后的数据")
    public AjaxResult get(@Min(1) @PathVariable("id") Long id) {
        entity.setId(id);
        return AjaxResult.success(entity);
    }

    @PostMapping("/getTest")
    public AjaxResult getTest(@RequestParam Long id) {
        entity.setId(id);
        return AjaxResult.success(entity);
    }


    @GetMapping("/noParam")
    public AjaxResult noParam() {
        return AjaxResult.success(entity);
    }


    @PostMapping("/page")
    @Decrypt
    @ApiOperation(value = "分页查询",notes = "解密入参")
    public AjaxResult page(@Validated @RequestBody PageQuery pageDomain) {
        log.info("pageDomain-----打印日志------{}", JSONUtil.toJsonStr(pageDomain));
        return AjaxResult.success(pageDomain);
    }


    @PostMapping("/add")
    public AjaxResult add(@Validated @RequestBody IosNoticeEntity iosNotice) {
        iosNotice.setId(RandomUtil.randomLong());
        return AjaxResult.success(iosNotice);
    }


    @PostMapping("/edit")
    public AjaxResult edit(@Validated @RequestBody IosNoticeEntity iosNotice) {

        return AjaxResult.success(iosNotice);
    }


}
