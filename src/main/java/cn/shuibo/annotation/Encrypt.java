package cn.shuibo.annotation;

import java.lang.annotation.*;

/**
 * 加密
 * Author:Bobby
 * DateTime:2019/4/9 16:45
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Encrypt{

}
