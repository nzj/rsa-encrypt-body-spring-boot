package cn.shuibo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *  参数解密配置
 * @Description  SecretKeyConfig
 * @Author guoyb
 * @Date   2022/12/15 17:53
 */
@ConfigurationProperties(prefix = "rsa.encrypt")
@Configuration
@Data
public class SecretKeyConfig{

    private String privateKey;

    private String publicKey;

    private String charset = "UTF-8";

    private boolean open = true;

    private boolean showLog = false;

}
