/*
+--------------------------------------------------------------------------
|   Mblog [#RELEASE_VERSION#]
|   ========================================
|   Copyright (c) 2014, 2015 mtons. All Rights Reserved
|   http://www.mtons.com
|
+---------------------------------------------------------------------------
*/
package cn.shuibo.model;

import lombok.Data;

import java.io.Serializable;

/**
 * 消息对象
 * @author langhsu
 */
@Data
public class AjaxResult<T> implements Serializable {
    private static final long serialVersionUID = -1491499610244557029L;

    public static final int SUCCESS = 0;
    public static final int ERROR = -1;

    /**
     * 状态码 0: success, -1: error
     */
    private int code;

    /**
     * 提示消息
     */
    private String message;

    /**
     * 结果数据
     */
    private T data;

    public AjaxResult(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> AjaxResult<T> success() {
        return success(null);
    }

    public static <T> AjaxResult<T> success(T data) {
        return success("操作成功", data);
    }

    public static <T> AjaxResult<T> successMessage(String message) {
        return success(message, null);
    }

    public static <T> AjaxResult<T> success(String message, T data) {
        return new AjaxResult<>(AjaxResult.SUCCESS, message, data);
    }

    public static <T> AjaxResult<T> failure(String message) {
        return failure(AjaxResult.ERROR, message);
    }

    public static <T> AjaxResult<T> failure(int code, String message) {
        return new AjaxResult<>(code, message, null);
    }

    public boolean isOk() {
        return code == SUCCESS;
    }

    public boolean isError() {
        return !isOk();
    }

}
