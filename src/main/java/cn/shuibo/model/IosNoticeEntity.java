package cn.shuibo.model;

import cn.shuibo.annotation.Sensitive;
import cn.shuibo.enums.SensitiveStrategy;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.Date;

@Data
public class IosNoticeEntity {
    private static final long serialVersionUID = 1L;


    /**
     * 消息ID,主键
     */
    private Long id;

    /**
     * 位置点名称
     */
    @Sensitive(strategy = SensitiveStrategy.USERNAME)
    private String name;

    /**
     * 位置点地址
     */
    @Sensitive(strategy = SensitiveStrategy.ADDRESS)
    private String address;

    /**
     * 创建时间
     */
    @JsonIgnore
    private Date createTime;
}
