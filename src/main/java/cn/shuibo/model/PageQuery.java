package cn.shuibo.model;

import lombok.Data;

/**
 * 分页查询参数
 *
 * @Author guoyb
 * @Date 2021/11/25  11:17
 */
@Data
public class PageQuery extends PageDomain {
    /**
     * 位置点名称
     */
    private String name;

    /**
     * 位置点地址
     */
    private String address;

}
