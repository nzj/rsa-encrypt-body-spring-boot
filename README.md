### 1.介绍
**rsa-encrypt-body-spring-boot**  
Spring Boot接口加密，可以对返回值、参数值通过注解的方式自动加解密
。  

### 2.使用方法
**Apache Maven**
```
<dependency>
  <groupId>cn.shuibo</groupId>
  <artifactId>rsa-encrypt-body-spring-boot</artifactId>
  <version>1.0.1.RELEASE</version>
</dependency>
```
**Gradle Groovy DSL**
```
implementation 'cn.shuibo:rsa-encrypt-body-spring-boot:1.0.1.RELEASE'
```
**Gradle Kotlin DSL**、**Scala SBT**、**Apache Ivy**、**Groovy Grape**、**Leiningen**、**Apache Buildr**、**Maven Central Badge**、**PURL**、**Bazel**方式请阅读[Spring Boot接口RSA自动加解密](https://www.shuibo.cn/102.html)
- **以Maven为例，在pom.xml中引入依赖**  
```
<dependency>
    <groupId>cn.shuibo</groupId>
    <artifactId>rsa-encrypt-body-spring-boot</artifactId>
    <version>1.0.1.RELEASE</version>
</dependency>
```
- **启动类Application中添加@EnableSecurity注解**

```
@SpringBootApplication
@EnableSecurity
public class DemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
```
- **在application.yml或者application.properties中添加RSA公钥及私钥**

```
rsa:
  encrypt:
    open: true # 是否开启加密 true  or  false
    showLog: true # 是否打印加解密log true  or  false
    publicKey: # RSA公钥
    privateKey: # RSA私钥
```
- **对返回值进行加密**

```
@Encrypt
@GetMapping("/encryption")
public TestBean encryption(){
    TestBean testBean = new TestBean();
    testBean.setName("shuibo.cn");
    testBean.setAge(18);
    return testBean;
}
```
- **对传过来的加密参数解密**

```
@Decrypt
@PostMapping("/decryption")
public String Decryption(@RequestBody TestBean testBean){
    return testBean.toString();
}
```

### 3.实现理论
- RequestBodyAdvice，针对所有以@RequestBody的参数，在读取请求body之前或者在body转换成对象之前可以做相应的增强。
我们处理了有参数和没有参数的情况，打印出请求类、方法、请求参数。注意：这里要加上@ControllerAdvice请求才能增强。

- ResponseBodyAdvice 接口是在 Controller 执行 return 之后，在 response 返回给客户端之前，执行的对 response 的一些处理，
可以实现对 response 数据的一些统一封装或者加密等操作。
1: supports  —— 判断是否要执行beforeBodyWrite方法，true为执行，false不执行  ——  通过supports方法，我们可以选择哪些类或哪些方法要对response进行处理，其余的则不处理。
2: beforeBodyWrite  ——  对 response 处理的具体执行方法。

-  解释：@RestControllerAdvice(“com.zqsign.app.privatearbitrate.controller.remote”)：
  表示com.zqsign.app.privatearbitrate.controller.remote此包下的所有响应对象都会经过此拦截器，并对响应体加上签名。


-  JsonSerializer序列化自定义注解,实现字段内容的脱敏
